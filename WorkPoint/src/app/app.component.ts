import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import jwt_decode from 'jwt-decode';
import { Socket } from 'ngx-socket-io';
@Component({
  selector: 'ws-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent implements OnInit {
  isloggedin = false;

  constructor(private authService: AuthService, private socket: Socket) {}

  ngOnInit(): void {
    const token = localStorage.getItem('token');
    const decoded = jwt_decode(token);
  }
}
