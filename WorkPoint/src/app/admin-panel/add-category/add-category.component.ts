import { Component, OnInit } from '@angular/core';
import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminApiService } from 'src/app/core/Services/admin-api.service';
import { MessageService } from 'src/app/core/Services/message.service';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'ws-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.less'],
})
export class AddCategoryComponent implements OnInit {
  searchValue = '';
  isSelected: Array<any> = [];
  isAnyFormAction: boolean | false;
  activeFormName: any;
  selectedCategory: any;
  selectedCategoryDetails: any = null;
  nodes = [];
  fileName: any;

  constructor(
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private adminService: AdminApiService,
    private message: MessageService,
    private modalService: NzModalService
  ) {}

  addCategoryForm = this.fb.group({
    categoryName: ['', Validators.required],
    image: [''],
    subCategories: this.fb.array([]),
  });

  updateCategoriesForm = this.fb.group({
    title: [''],
    key: [''],
    children: this.fb.array([]),
  });

  ngOnInit(): void {
    this.spinner.show();
    this.adminService.getCategory().subscribe((res: any) => {
      this.nodes = res;
      this.spinner.hide();
    });
  }

  formSelected(formName: any): void {
    this.isAnyFormAction = true;
    this.activeFormName = formName;
  }

  get subCategories(): any {
    return this.addCategoryForm.get('subCategories') as FormArray;
  }

  get selectedSubCategories(): any {
    return this.updateCategoriesForm.get('children') as FormArray;
  }

  addCategory(): void {
    this.subCategories.push(this.fb.control(''));
  }

  addCategoryUpdate(): void {
    this.selectedSubCategories.push(this.fb.control(''));
  }

  deleteCategory(index: any): void {
    this.subCategories.removeAt(index);
  }

  deleteCategoryUpdate(index: any): void {
    this.selectedSubCategories.removeAt(index);
  }

  onChooseFile($event: any): void {
    this.fileName = $event.target.files[0];
  }

  onAddCategory(): void {
    this.spinner.show();
    const addedCategory = {
      title: this.addCategoryForm.value.categoryName,
      key: this.addCategoryForm.value.categoryName,
      subService: [],
    };
    this.addCategoryForm.value.subCategories.forEach((subCat: any) => {
      if (subCat !== '') {
        addedCategory.subService.push({
          title: subCat,
          key: uuidv4(),
        });
      }
    });
    this.nodes = [...this.nodes, addedCategory];
    const fb = new FormData();
    fb.append('title', addedCategory.title);
    fb.append('subServices', JSON.stringify(addedCategory.subService));
    fb.append('image', this.fileName);
    this.adminService.addCategory(fb).subscribe(
      (res: any) => {
        if (res) {
          this.spinner.hide();
          this.nodes = res.data;
          this.message.showMessage('success', res.msg);
          this.addCategoryForm.reset();
          this.subCategories.clear();
        }
      },
      (err: any) => {
        if (err) {
          this.spinner.hide();
          this.message.showMessage('error', err);
        }
      }
    );
  }

  onCategoryChange(): void {
    this.nodes.forEach((category: any) => {
      if (category.key === this.selectedCategory) {
        this.selectedCategoryDetails = category;
      }
    });
    this.updateCategoriesForm.patchValue({
      title: this.selectedCategoryDetails?.title,
    });
    this.updateCategoriesForm.patchValue({
      key: this.selectedCategoryDetails?.key,
    });
    this.selectedSubCategories.clear();
    this.selectedCategoryDetails.children.forEach((child: any) => {
      this.selectedSubCategories.push(this.fb.control(child.title));
    });
  }

  onSaveCategory(): void {
    const updatedCategory = {
      title: this.updateCategoriesForm.value.title,
      key: this.updateCategoriesForm.value.key,
      children: [],
    };

    this.updateCategoriesForm.value.children.forEach((subCat: any) => {
      if (subCat !== '') {
        updatedCategory.children.push({
          title: subCat,
          key: subCat,
        });
      }
    });

    for (let i = 0; i < this.nodes.length; i = i + 1) {
      if (this.nodes[i].key === this.updateCategoriesForm.value.key) {
        this.nodes.splice(i, 1);
      }
    }
    this.nodes = [...this.nodes, updatedCategory];

    this.updateCategoriesForm.reset();
    this.selectedSubCategories.clear();
  }

  deleteSubCategory(parentId: any, childId: any): void {
    let nzContents = '';
    if (childId === null) {
      nzContents = `<b class="delalert">If you delete the parent category, it's sub-category are also deleted </b>`;
    }

    this.modalService.confirm({
      nzTitle: 'Do you Want to delete these items?',
      nzContent: nzContents,
      nzOnOk: () => {
        this.spinner.show();
        this.adminService.deleteSubCategory({ parentId, childId }).subscribe(
          (res: any) => {
            this.spinner.hide();
            this.nodes = res.data;
            this.message.showMessage('success', res.message);
          },
          (err: any) => {
            this.spinner.hide();
            this.message.showMessage('error', err);
          }
        );
      },
    });
  }

  addLeaf(): void {
    this.nodes.forEach((ele) => {
      ele.key = ele._id;
      ele.children = ele.subService;
      ele.subService.forEach((item) => {
        // tslint:disable-next-line: no-string-literal
        if (item['isLeaf'] === undefined) {
          // tslint:disable-next-line: no-string-literal
          item['isLeaf'] = true;
        }
      });
    });
  }
}
