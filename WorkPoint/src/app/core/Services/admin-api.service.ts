import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AdminApiService {
  constructor(private http: HttpClient) {}

  addCategory(data: any): Observable<any> {
    return this.http.post(
      `${environment.nodeServerUrl}admin/addcategory`,
      data
    );
  }

  getCategory(): Observable<any> {
    return this.http.get(`${environment.nodeServerUrl}admin/getctegory`);
  }

  deleteSubCategory(params: any): Observable<any> {
    return this.http.post(
      `${environment.nodeServerUrl}admin/deleteSubcategory`,
      {
        params,
      }
    );
  }
}
