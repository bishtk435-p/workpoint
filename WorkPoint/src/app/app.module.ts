import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeModule } from './home/home.module';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { AuthModule } from './auth/auth.module';
import { WsCommonModule } from '../app/common/common.module';
import { CoreModule } from '../app/core/core.module';
import { AuthGaurdGuard } from './auth/AuthGaurd/auth-gaurd.guard';
import { TokenInterceptorService } from './auth/tokenInterceptor/token-interceptor.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { PaymentComponent } from './common/Components/checkout-page/payment/payment.component';
import { RequestedServicesComponent } from './seller-panel/requested-services/requested-services.component';

const config: SocketIoConfig = { url: 'https://enigmatic-wave-35007.herokuapp.com/', options: {} };
registerLocaleData(en);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    HomeModule,
    NzButtonModule,
    WsCommonModule,
    AuthModule,
    CoreModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    SocketIoModule.forRoot(config),
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    AuthGaurdGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
