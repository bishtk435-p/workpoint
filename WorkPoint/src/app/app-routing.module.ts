import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
    pathMatch: 'full',
  },
  {
    path: 'seller',
    loadChildren: () =>
      import('./seller-panel/seller-panel.module').then(
        (m) => m.SellerPanelModule
      ),
  },
  {
    path: 'service-provider',
    loadChildren: () =>
      import('./service-provider/service-provider.module').then(
        (m) => m.ServiceProviderModule
      ),
  },
  {
    path: 'serviceProvider/:serviceid',
    loadChildren: () =>
      import('./select-service-provider/select-service-provider.module').then(
        (m) => m.SelectServiceProviderModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
