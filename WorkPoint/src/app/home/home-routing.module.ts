import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGaurdGuard } from '../auth/AuthGaurd/auth-gaurd.guard';
import { ChangePasswordComponent } from '../common/Components/change-password/change-password.component';
import { CanclePaymentComponent } from '../common/Components/checkout-page/cancle-payment/cancle-payment.component';
import { CheckoutPageComponent } from '../common/Components/checkout-page/checkout-page.component';
import { PaymentComponent } from '../common/Components/checkout-page/payment/payment.component';
import { ErrorPageComponent } from '../common/Components/error-page/error-page.component';
import { ForgetPasswordComponent } from '../common/Components/forget-password/forget-password.component';
import { ResetPasswordComponent } from '../common/Components/reset-password/reset-password.component';
import { HomeComponent } from '../home/home/home.component';
import { ContactusComponent } from './contactus/contactus.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('../admin-panel/admin-panel.module').then(
        (m) => m.AdminPanelModule
      ),
    canActivate: [AuthGaurdGuard],
  },
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'forgetpassword',
    component: ForgetPasswordComponent,
  },
  {
    path: 'resetpassword',
    component: ResetPasswordComponent,
  },
  {
    path: 'changepassword',
    component: ChangePasswordComponent,
    pathMatch: 'full',
    canActivate: [AuthGaurdGuard],
  },

  {
    path: 'checkout',
    component: CheckoutPageComponent
  },
  {
    path:'payment',
    component: PaymentComponent
  },
  {
    path:'cancelPayment',
    component: CanclePaymentComponent
  },
  {
    path: 'contact',
    component: ContactusComponent
  },
  {
    path: '**',
    component: ErrorPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
