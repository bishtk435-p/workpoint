import { Component, OnInit } from '@angular/core';
// import { AvailableService } from '../../dummy-data/available-services';
import { ApiService } from '../../core/Services/api.service';
@Component({
  selector: 'ws-select-service',
  templateUrl: './select-service.component.html',
  styleUrls: ['./select-service.component.less'],
})
export class SelectServiceComponent implements OnInit {
  availableServices: any;
  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.apiService.getAvailableServices('sort=true').subscribe((res: any) => {
      this.availableServices = res;
    });
  }
}
