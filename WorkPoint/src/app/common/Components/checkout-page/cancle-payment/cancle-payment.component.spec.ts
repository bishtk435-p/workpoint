import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CanclePaymentComponent } from './cancle-payment.component';

describe('CanclePaymentComponent', () => {
  let component: CanclePaymentComponent;
  let fixture: ComponentFixture<CanclePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CanclePaymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CanclePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
