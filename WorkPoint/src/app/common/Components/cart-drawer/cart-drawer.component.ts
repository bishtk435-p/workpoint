import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { NzDrawerPlacement, NzDrawerRef } from 'ng-zorro-antd/drawer';
import { AdminApiService } from 'src/app/core/Services/admin-api.service';
import { CartService } from '../../../core/Services/cart.service';

@Component({
  selector: 'ws-cart-drawer',
  templateUrl: './cart-drawer.component.html',
  styleUrls: ['./cart-drawer.component.less'],
})
export class CartDrawerComponent implements OnInit {
  cateList;
  selectedServiceProviders: any;

  data: any;

  constructor(
    private cartService: CartService,
    private drawerRef: NzDrawerRef,
    private adminService: AdminApiService
  ) {}

  ngOnInit(): void {
    this.adminService.getCategory().subscribe((res) => {
      this.cateList = res;
      console.log('response catalist', res);
    });

    this.cartService.selectedServiceProviders$.subscribe((res: any) => {
      console.log('response', res);
      this.selectedServiceProviders = res;
    });
  }

  getImage(url: any) {
    return `http://localhost:3000${url}`;
  }

  deleteItem(id): any {
    this.cartService.deleteItemFromCart(id);
  }

  removeAllService(): any {
    this.closeDarwer();
    this.cartService.removeAllItems();
  }
  closeDarwer() {
    this.drawerRef.close();
  }

  getSubcatename(id): any {
    this.cateList.forEach((element) => {
      element.subService.forEach((ele) => {
        if (ele.key === id) {
          return ele.title;
        }
      });
    });
  }
}
