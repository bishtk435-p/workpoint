import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ws-feature-card',
  templateUrl: './feature-card.component.html',
  styleUrls: ['./feature-card.component.less']
})
export class FeatureCardComponent implements OnInit {
  @Input() title: any;
  @Input() discription: any;
  constructor() { }

  ngOnInit(): void {
  }

}
