import { Component, Input, OnInit } from '@angular/core';
import { Profile } from '../../../models/profile.model';
import { CartService } from '../../../core/Services/cart.service';
import { ServiceProviderService } from 'src/app/service-provider/service-provider.service';
import { AdminApiService } from 'src/app/core/Services/admin-api.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { SignupComponent } from 'src/app/auth/signup/signup.component';
import { SelectServiceProviderFormComponent } from '../select-service-provider-form/select-service-provider-form.component';

@Component({
  selector: 'ws-profile-info-card',
  templateUrl: './profile-info-card.component.html',
  styleUrls: ['./profile-info-card.component.less'],
})
export class ProfileInfoCardComponent implements OnInit {
  @Input() profileDetail: Profile;
  @Input() category: any;
  isSelected = false;

  constructor(
    private cartService: CartService,
    private adminservice: AdminApiService,
    private modalService: NzModalService
  ) {
    // this.cartService.isslected$.subscribe(
    //   res => this.isSelected = res,
    // );
    // this.cartService.emitFormValue.subscribe(
    //   res =>{
    //     const ProfileData = {
    //       profileDetail : this.profileDetail,
    //       UserFormData : res
    //     };
    //     this.onProfileClicked(ProfileData);
    //   }
    // );
  }

  ngOnInit(): void {
    this.cartService.selectedServiceProviders$.subscribe((item: any) => {
      let isFound = false;
      item.forEach((e: any) => {
        if (JSON.stringify(e.serviceProvider._id) === JSON.stringify(this.profileDetail._id)) {
          isFound = true;
        }
      });
      this.isSelected = isFound;
    });
  }

  onProfileClicked(ProfileData): void {
    console.log('this profile data', ProfileData)
    this.isSelected
      ? this.cartService.deleteItemFromCart(ProfileData.profileDetail._id)
      : this.cartService.addItemToCart(ProfileData);
  }

  getImage(url: any) {
    return `http://localhost:3000${url}`;
  }

  openSelectForm(): any {
    this.modalService.create({
      nzTitle: null,
      nzFooter: null,
      nzMaskClosable: false,
      // nzWidth: 900,
      // nzBodyStyle: {top:"0px",height: "600px"},
      nzComponentParams: {data: this.profileDetail , edit: false},
      nzContent: SelectServiceProviderFormComponent,
    });
  }
  deselectService(id): any{
    this.cartService.deleteItemFromCart(id);
  }
}
