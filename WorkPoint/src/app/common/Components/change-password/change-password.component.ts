import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/Services/api.service';
import { MessageService } from 'src/app/core/Services/message.service';

@Component({
  selector: 'ws-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less'],
})
export class ChangePasswordComponent implements OnInit {
  loader = false;
  accept = true;
  passwordVisible = false;
  passwordVisible1 = false;
  passwordVisible2 = false;
  passwordMatch = null;
  password?: string;

  passwordrex = new RegExp(
    '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{8,}'
  );

  lowercaseregex = new RegExp('(?=.*[a-z])');
  uppercaseregx = new RegExp('(?=.*[A-Z])');
  digitregex = new RegExp('(?=.*[0-9])');
  specialcharregex = new RegExp('(?=.*[!,@,#,$,%,&])');
  changePassword: any = '';

  constructor(
    private apiService: ApiService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.changePassword = this.route.snapshot.queryParams.changepassword;
  }

  checkFormData(value: any): boolean {
    if (!this.lowercaseregex.test(value.newpassword)) {
      this.passwordMatch =
        'Password must contain at least one lower case letter';
      return false;
    } else if (!this.uppercaseregx.test(value.newpassword)) {
      this.passwordMatch =
        'Password must contain at least one upper case letter';
      return false;
    } else if (!this.digitregex.test(value.newpassword)) {
      this.passwordMatch = 'Password must contain at least one digit letter';
      return false;
    } else if (!this.specialcharregex.test(value.newpassword)) {
      this.passwordMatch =
        'Password must contain at least one special character';
      return false;
    } else if (value.newpassword.length < 8) {
      this.passwordMatch = 'Password must contain at least content 8 character';
      return false;
    } else if (value.newpassword !== value.confirmPassword) {
      this.passwordMatch = 'Password Not Matched';
      return false;
    } else if (value.password === value.newpassword) {
      this.messageService.showMessage(
        'error',
        'Old and new password can not be same'
      );
    } else if (!this.accept) {
      return false;
    } else {
      return true;
    }
  }

  onSubmit(formValue: any): void {
    if (this.checkFormData(formValue)) {
      formValue.userid = localStorage.getItem('userid');
      this.apiService.changePassword(formValue).subscribe(
        (res: any) => {
          if (res) {
            this.messageService.showMessage('success', res);
          }
        },
        (err: any) => {
          if (err.status === 500) {
            this.messageService.showMessage('error', 'Unauthorized request');
            this.router.navigate(['/']);
          }
          if (err.status !== 500) {
            this.messageService.showMessage('error', err.error);
          }
        }
      );
    }
  }
}
