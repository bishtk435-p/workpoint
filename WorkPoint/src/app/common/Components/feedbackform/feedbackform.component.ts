import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { ApiService } from 'src/app/core/Services/api.service';

@Component({
  selector: 'ws-feedbackform',
  templateUrl: './feedbackform.component.html',
  styleUrls: ['./feedbackform.component.less'],
})
export class FeedbackformComponent implements OnInit {
  @Input() userData: any;

  tooltips = ['Terrible', 'Bad', 'Normal', 'Good', 'Wonderful'];
  value = 0;

  constructor(
    private modal: NzModalRef,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private apiService: ApiService
  ) {}

  feedbackForm = this.fb.group({
    rating: [''],
    behaviour: [''],
    workAccuracy: [''],
    overallExperiance: [''],
    comment: [''],
  });

  ngOnInit(): void {}

  onSubmit(): void {
    this.spinner.show();
    const taskId = this.userData._id;
    const serviceProviderId = this.userData.requestedTo;
    const submitedby = this.userData.requestBy;
    const status = this.userData.status;
    const tempObj = {
      taskId,
      serviceProviderId,
      submitedby,
      status,
      data: this.feedbackForm.value,
    };
    console.log('tempObjtempObjtempObj', tempObj);

    this.apiService.saveFeedback(tempObj).subscribe(
      (res: any) => {
        console.log('$#$#$', res);

        if (res) {
          this.apiService.updateItem.emit(res);
          this.spinner.hide();
          this.closeModal();
          Swal.fire(
            'Thank you for your valuable feedback!',
            'Your feebacks help us to improve our services',
            'success'
          );
        }
      },
      (err) => {
        console.log('sdhfsdyhfoweqa.,z', err);
        this.spinner.hide();
        this.closeModal();
        Swal.fire('OOps!', 'Please try again...', 'error');
      }
    );
  }

  closeModal(): void {
    this.modal.destroy();
  }
}
