import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectServiceProviderFormComponent } from './select-service-provider-form.component';

describe('SelectServiceProviderFormComponent', () => {
  let component: SelectServiceProviderFormComponent;
  let fixture: ComponentFixture<SelectServiceProviderFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectServiceProviderFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectServiceProviderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
