import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminApiService } from 'src/app/core/Services/admin-api.service';
import { ServiceProviderService } from 'src/app/service-provider/service-provider.service';

@Component({
  selector: 'ws-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.less'],
})
export class FiltersComponent implements OnInit {
  selectesServices: any = [];
  radioValue = '';
  rangeValue = [0, 1000];

  category;

  constructor(
    private adminservice: AdminApiService,
    private service: ServiceProviderService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.adminservice.getCategory().subscribe((res) => {
      console.log(res);
      this.category = res;
    });
  }

  getAllServices(): void {
    this.spinner.show();
    this.service.getAllServiceProvides().subscribe((res: any) => {
      this.service.filterServiceProvider.emit(res);
      this.spinner.hide();
    });
  }

  onSelectService(selectedoption: any): void {
    this.selectesServices = selectedoption;
    if (!this.selectesServices.length) {
      this.getAllServices();
    } else {
      this.applyFilter();
    }
  }

  ClearFilter(): void {
    this.selectesServices = [];
  }

  onChange(value: number): void {
    console.log(`onChange: ${value}`);
  }

  onAfterChange(value: number[] | number): void {
    console.log(`onAfterChange: ${value}`);
  }

  applyFilter(): void {
    this.spinner.show();
    this.service
      .filterServiceProvides(this.selectesServices)
      .subscribe((res: any) => {
        this.service.filterServiceProvider.emit(res);
        this.spinner.hide();
      });
  }
}
