import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { DisplayServiceProviderComponent } from './display-service-provider/display-service-provider.component';
import { SelectedServiceProviderComponent } from './selected-service-provider/selected-service-provider.component';

const routes: Routes = [
  {
    path: '',
    component: DisplayServiceProviderComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), NzMenuModule],
  exports: [RouterModule],
})
export class ServicesRoutingModule {}
