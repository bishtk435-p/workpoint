import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminApiService } from 'src/app/core/Services/admin-api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServiceProviderService } from 'src/app/service-provider/service-provider.service';
import { query } from '@angular/animations';
// import { ServiceProvidersDetails } from '../../dummyData/serviceProviders';
@Component({
  selector: 'ws-display-service-provider',
  templateUrl: './display-service-provider.component.html',
  styleUrls: ['./display-service-provider.component.less'],
})
export class DisplayServiceProviderComponent implements OnInit {
  selectedServiceId: any;
  category: any;

  constructor(
    private activatedRouter: ActivatedRoute,
    private apiService: ServiceProviderService,
    private adminService: AdminApiService,
    private spinner: NgxSpinnerService,
    private serviceApi: ServiceProviderService,
    private route: Router
  ) {
    this.activatedRouter.params.subscribe((param: any) => {
      this.selectedServiceId = param.serviceid;
      this.getServices();
    });
  }

  profileInfo = [];

  ngOnInit(): void {
    window.scroll(0, 0);
    this.serviceApi.filterServiceProvider.subscribe((res: any) => {
      this.profileInfo = res;
    });
    // this.getServices();
    this.getCategory();
  }

  ngOnChanges(): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.getServices();
    console.log('change');
  }

  getServices(): any {
    this.spinner.show();
    this.apiService
      .getServices(this.selectedServiceId)
      .subscribe((res: any) => {
        this.profileInfo = res;
        this.spinner.hide();
      });
  }

  getCategory(): void {
    this.spinner.show;
    this.adminService.getCategory().subscribe((res: any) => {
      this.category = res;
      this.spinner.hide();
    });
  }
}
