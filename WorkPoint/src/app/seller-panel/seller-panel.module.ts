import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WsCommonModule } from '../common/common.module';
import { SellerPanelRoutingModule } from './seller-panel-routing.module';
import { SellerDashboardComponent } from './seller-dashboard/seller-dashboard.component';
import { RequestedServicesComponent } from './requested-services/requested-services.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AntDesignModule } from '../ant-design/ant-design.module';

@NgModule({
  declarations: [SellerDashboardComponent, RequestedServicesComponent],
  imports: [
    CommonModule,
    SellerPanelRoutingModule,
    WsCommonModule,
    FormsModule,
    ReactiveFormsModule,
    AntDesignModule,
  ],
})
export class SellerPanelModule {}
