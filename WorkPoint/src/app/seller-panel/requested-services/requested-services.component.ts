import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { FormBuilder } from '@angular/forms';
import { ServiceProviderService } from 'src/app/service-provider/service-provider.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'src/app/core/Services/message.service';
import { FeedbackformComponent } from 'src/app/common/Components/feedbackform/feedbackform.component';
import { ApiService } from 'src/app/core/Services/api.service';

@Component({
  selector: 'ws-requested-services',
  templateUrl: './requested-services.component.html',
  styleUrls: ['./requested-services.component.less'],
})
export class RequestedServicesComponent implements OnInit {
  serviceStatus = ['accepted', 'rejected', 'pending', 'success', 'done'];
  filterForm = this.fb.group({
    status: [''],
  });
  tableDataToShow = [];
  userRole: string;
  userid: string;

  constructor(
    private modalService: NzModalService,
    private fb: FormBuilder,
    private service: ServiceProviderService,
    private spinner: NgxSpinnerService,
    private message: MessageService,
    private apiService: ApiService
  ) {}

  ngOnInit(): void {
    this.userRole = localStorage.getItem('userRole');
    this.getServiceRequest();
    this.apiService.updateItem.subscribe(() => {
      this.getServiceRequest();
    });
  }

  acceptServiceRequest(requestId: any): void {
    this.modalService.confirm({
      nzTitle: 'Accept',
      nzFooter: null,
      nzClosable: false,
      nzContent: 'Do you want to accept this service request?',
      nzOkText: 'Accept',
      nzCancelText: 'Cancel',
      nzOnOk: () => {
        this.spinner.show();
        this.service
          .approveRequest({ requestId, action: 'accepted' })
          .subscribe((res: any) => {
            this.getServiceRequest();
            this.spinner.hide();
            this.message.showMessage('success', res.message);
            // this.tableDataToShow = res.data;
          });
      },
      nzOnCancel: () => {
        this.modalService.closeAll();
      },
    });
  }

  rejectServiceRequest(requestId: any): void {
    this.modalService.confirm({
      nzTitle: 'Reject',
      nzFooter: null,
      nzClosable: false,
      nzContent: 'Do you want to reject this service request?',
      nzOkText: 'Reject',
      nzCancelText: 'Cancel',
      nzOnOk: () => {
        this.spinner.show();
        this.service
          .approveRequest({ requestId, action: 'rejected' })
          .subscribe((res: any) => {
            this.getServiceRequest();
            this.spinner.hide();
            this.message.showMessage('success', res.message);
            // this.tableDataToShow = res.data;
          });
      },
      nzOnCancel: () => {
        this.modalService.closeAll();
      },
    });
  }

  declineServiceRequest(requestId: any): void {
    this.modalService.confirm({
      nzTitle: 'Decline',
      nzFooter: null,
      nzClosable: false,
      nzContent: 'Do you want to reject this service request?',
      nzOkText: 'Decline',
      nzCancelText: 'Cancel',
      nzOnOk: () => {
        this.spinner.show();
        this.service
          .approveRequest({ requestId, action: 'declined' })
          .subscribe((res: any) => {
            this.getServiceRequest();
            // this.spinner.hide();
            this.message.showMessage('success', res.message);
            // this.tableDataToShow = res.data;
          });
      },
      nzOnCancel: () => {
        this.modalService.closeAll();
      },
    });
  }

  getServiceRequest(): void {
    this.spinner.show();
    this.userid = localStorage.getItem('userid');
    switch (this.userRole) {
      case 'serviceprovider': {
        this.service.getServicesRequest(this.userid).subscribe(
          (res: any) => {
            this.spinner.hide();
            if (res.length === 0) {
              this.tableDataToShow = [];
            } else {
              this.tableDataToShow = res;
            }
          },
          (err: any) => {
            this.spinner.hide();
            this.tableDataToShow = [];
          }
        );
        break;
      }
      case 'user': {
        this.service.getPostedRequest(this.userid).subscribe(
          (res: any) => {
            this.spinner.hide();
            if (res.length === 0) {
              this.tableDataToShow = [];
            } else {
              this.tableDataToShow = res;
            }
          },
          (err: any) => {
            this.spinner.hide();
            this.tableDataToShow = [];
          }
        );
        break;
      }
      default: {
        this.spinner.hide();
        break;
      }
    }
  }

  submitFeedback(data: any) {
    this.modalService.create({
      nzContent: FeedbackformComponent,
      nzTitle: null,
      nzFooter: null,
      nzClosable: null,
      nzBodyStyle: { top: '0px' },
      nzComponentParams: { userData: data },
    });
  }

  onServiceRequestChange(): void {
    this.spinner.show();
    if (this.userRole === 'user') {
      if (this.filterForm.controls.status.value) {
        this.service
          .filterRequest({
            userid: this.userid,
            status: this.filterForm.controls.status.value,
          })
          .subscribe(
            (res: any) => {
              this.spinner.hide();
              if (res.length === 0) {
                this.tableDataToShow = [];
              } else {
                this.tableDataToShow = res;
              }
            },
            (err: any) => {
              this.spinner.hide();
              this.tableDataToShow = [];
            }
          );
      } else {
        this.getServiceRequest();
      }
    } else if (this.userRole === 'serviceprovider') {
      if (this.filterForm.controls.status.value) {
        this.service
          .filterProviderRequest({
            userid: this.userid,
            status: this.filterForm.controls.status.value,
          })
          .subscribe(
            (res: any) => {
              this.spinner.hide();
              if (res.length === 0) {
                this.tableDataToShow = [];
              } else {
                this.tableDataToShow = res;
              }
            },
            (err: any) => {
              this.spinner.hide();
              this.tableDataToShow = [];
            }
          );
      } else {
        this.getServiceRequest();
      }
    }
  }
}
